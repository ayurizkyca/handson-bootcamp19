const fetchUserData = () => {
    return new Promise((resolve, reject) => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => {
                if (!response.ok) {
                    reject(new Error(`Failed to fetch user data. Status: ${response.status}`));
                }
                return response.json();
            })
            .then(data => {
                resolve(data);
            })
            .catch(error => {
                reject(new Error(`Failed to fetch user data: ${error.message}`));
            });
    });
};

const consumeUserData = async () => {
    try {
        const userData = await fetchUserData();
        console.log("User data:", userData);
    } catch (error) {
        console.error("Error:", error.message);
    }
};

consumeUserData();
