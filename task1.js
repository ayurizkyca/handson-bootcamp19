// menghitung jumlah element
let numbers = [3,5,6,7,8,8,10,5,7,10];
let numbersLength = numbers.length;
console.log("Numbers Length " + numbersLength);

// menhitung nilai min max
let max = Math.max(...numbers);
console.log("Max : " + max);

let min = Math.min(...numbers);
console.log("Min : " + min); 

//menggabungkan 2 array
let numbers2 = [1,2,1,2,12];
let mergeNumbers = [...numbers, ...numbers2];
console.log("Merge Array Value : " + mergeNumbers);

//menghapus angka duplikat
let removeDuplicateValue = [...new Set(mergeNumbers)];
console.log("Array without duplicate value : " + removeDuplicateValue); 