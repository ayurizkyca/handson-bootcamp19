const {from} = require('rxjs');
const {filter, map, reduce } = require('rxjs/operators');

let numbers =[1,2,3,4,5,6,7];

const observeArray = from(numbers)

//map
const mapNumbers = observeArray.pipe(
    map(i => i * 2)
)

mapNumbers.subscribe(
    result => console.log("map hasil : " + result)
)

//filter
const filterNumbers = observeArray.pipe(
    filter(value => value % 2 ==0)
)

filterNumbers.subscribe(
    result => console.log("filter hasil : " + result)
)

//reduce
const reduceNumbers = observeArray.pipe(
    reduce((acc, curr) => acc + curr, 0)
)

reduceNumbers.subscribe(
    result => console.log("reduce hasil : " + result)
)
