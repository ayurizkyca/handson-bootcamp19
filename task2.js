//membuat array bersikan objeck karyawan (nama, posisi, gaji)
let employees = [
    {
        name : "Diah",
        posistion : "bankir",
        salary : 2000000
    },
    {
        name : "Dwi",
        posistion : "programmer",
        salary : 7000000
    },
    {
        name : "Cahya",
        posistion : "teacher",
        salary : 3000000
    }
]

const showEmployees = (employees) => {
    employees.forEach((employee) => {
        console.log(`Name: ${employee.name}, Position: ${employee.position}, Salary: ${employee.salary}`);
    });
}
showEmployees(employees);


//arrow and callback function
const listEmployees = (employee, callback) => {
    callback(employee);
};

//menampilkan list karyawan dengan gaji tertentu
const showESalaryAbove = (employee, salaryV) => {
    console.log(`====Employees with salary above  ${salaryV}====`);
    employee.forEach((e) => {
        if (e.salary >= salaryV) {
            console.log(`Name: ${e.name}, Position: ${e.position} , Salary : ${e.salary}`);
        }
    });
};

listEmployees(employees, showESalaryAbove.bind(null,employees, 3000000));

//menampilkan total gaji seluruh kaeyawan
const showTotalSalary = (employee) => {
    let totalSalary = employee.reduce((total, e) => total + e.salary, 0);
    console.log(`Total Salary : ${totalSalary}`);
}

listEmployees(employees, showTotalSalary);
